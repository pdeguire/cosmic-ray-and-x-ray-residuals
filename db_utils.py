import sqlite3
import os.path

db_type_map = { 'TEXT':str, 'INTEGER':int, 'REAL':float}
def convert(val, t):
    """Convert a value to the specified type"""
    if val == None: return None
    try:  return t(val)
    except ValueError:  return None

class Row(object):
    def __repr__(self):
        return str(self.__dict__)
    
class DB(object):
    def __init__(self, fname):
        self.verbose=False
        if fname is not None:
            self.open(fname)
    def open(self, fname):
        if not os.path.isfile(fname):
            raise Exception("Requested DB file '%s' does not exist." % (fname))
        self.conn = sqlite3.connect(fname)
    def close(self):
        self.conn.close()
    def run(self, cmd):
        c = self.conn.cursor()
        if self.verbose:
            print('SQL:', cmd)
        return c.execute(cmd)
    def write(self, cmd):
        self.run(cmd)
        self.conn.commit()
    def insert(self, tname, entry):
        col_types = self.get_col_types(tname)
        cols = set(col_types.keys())
        for key in entry.__dict__.keys():
            if key not in cols:
                raise Exception('INSERT_ERROR: Column "%s" not in table "%s".' % (key, tname))
        
        form_vals = []
        for k, v in entry.__dict__.items():
            if v is not None:
                t = col_types[k]
                if t==str: v = "'%s'" % (v)
                else: v = str(v)
                form_vals.append((k,v))
        col_list, val_list = zip(*form_vals)
        col_list, val_list = ','.join(col_list), ','.join(val_list)

        cmd = 'INSERT INTO %s (%s) VALUES (%s)' % (tname, col_list, val_list)
        self.write(cmd)


    def read(self, cmd):
        return list(self.run(cmd))
    def get_table_info(self, tname):
        tinfo = self.read('PRAGMA TABLE_INFO(%s);' % (tname))
        if len(tinfo)==0: raise Exception("Table '%s' does not exist." % (table_name))
        else: return tinfo;
    def column_exists(self, tname, cname):
        return (cname in [row[1] for row in self.get_table_info(tname)])
    def add_column(self, tname, cname, ctype):
        if not self.column_exists(tname, cname):
            print('Adding column %s (type %s) to table %s.' % (cname, ctype, tname))
            self.write('ALTER TABLE %s ADD COLUMN %s %s;' % (tname, cname, ctype));    
        else: print('WARNING: Column %s already exists in table %s.' % (cname, tname))
    def get_col_types(self, tname):
        return { row[1] : db_type_map[row[2]] for row in self.get_table_info(tname) }
    def select(self, tname, cols, cut=None):
        cmd = 'SELECT %s FROM %s' % (','.join(cols), tname)
        if cut is not None:
            cmd += ' WHERE '+cut
        res = self.read(cmd)
        types = self.get_col_types(tname)
        row_list = []
        for row in res:
            row_list.append( Row())
            for col_idx, col_name in enumerate(cols):
                setattr(row_list[-1], col_name, convert(row[col_idx], types[col_name]) )  
        return row_list
    def update(self, tname, cols, vals, cut):
        types = self.get_col_types(tname)
        vals = [ ("'%s'" % (val)) if types[col_name]==str else val for col_name, val in zip(cols, vals) ]
        cmd = ', '.join([ '%s=%s' % (col_name, str(val)) for col_name, val in zip(cols, vals) ])
        cmd = 'UPDATE %s SET %s WHERE %s;' % (tname, cmd, cut)
        self.write(cmd)

    
    
