###################################################################################
###################################################################################

# pars: [offset, slope, lag1, lag2, lag3]
def chi2_cosm_xray(pars, dico_x, dico_c, gv):   
    chi2 = 0
    list_sum = sum_others(gv, dico_x)
    dico_others = parse_other_layers(dico_x, gv)
    # XRAY tuple : (pt_id, x, y, dy_xray, y_cmm, dy_xray_error, dy_cmm_error)
    # COSMICS tuple : (pt_id, fixed_layer1, fixed_layer2, mean)
    for pt_id, x, y, dy_xray, y_cmm, dy_xray_error, dy_cmm_error in dico_x[gv]:        
        # chi2 in BenOC
        y_cmm_corr = y_cmm + pars[0] + pars[1]*x
        dy_cmm_corr = y_cmm_corr - y
        diff = dy_cmm_corr-dy_xray
        chi2 += diff**2 / (dy_xray_error**2+dy_cmm_error**2)
        # Dictionary residuals cosmics
        dico_res_c = {}
        for pt_id_c, fl1, fl2, mean_c in dico_c[gv]:
            if pt_id_c == pt_id: dico_res_c[fl1+fl2] = mean_c
        # Dictionary residuals xray
        dico_sim = find_similar(pt_id, dico_others)
        dico_res_x = {}
        for sim1 in dico_sim:
            for sim2 in dico_sim:
                if sim1 > sim2:
                    y_track = y_otherlay_xray(gv, sim1, sim2, dico_sim)
                    res_xr = y_cmm_corr - y_track
                    dico_res_x[sim1+sim2] = res_xr
        # Lagrange multipliers
        for sum2l in list_sum:
            if sum2l in dico_res_c and sum2l in dico_res_x:
                ind_lag = 2 + list_sum.index(sum2l)
                chi2 += pars[ind_lag] * (dico_res_c[sum2l] - dico_res_x[sum2l])
    return chi2

###################################################################################
###################################################################################

###################################################################################
###################################################################################

def chi2_cosm_xray_flcorr(pars, dico_x, dico_c, gv):   
    chi2 = 0
    dico_others = parse_other_layers(dico_x, gv)
    # XRAY tuple : (pt_id, x, y, dy_xray, y_cmm, dy_xray_error, dy_cmm_error)
    # COSMICS tuple : (pt_id, fixed_layer1, fixed_layer2, mean)
    for pt_id, x, y, dy_xray, y_cmm, dy_xray_error, dy_cmm_error in dico_x[gv]:
        dico_sim = find_similar(pt_id, dico_others)
        res_c = None
        for pt_id_c, fl1, fl2, mean_c in dico_c[gv]:
            if pt_id_c == pt_id and fl1+fl2 == sum(list(dico_others.keys())): res_c = mean_c
        if len(list(dico_sim.keys())) == 2 and res_c != None:
            dico_corr={}
            # Chi2 chosen gv
            y_cmm_corr = y_cmm + pars[0] + pars[1]*x
            dico_corr[gv] = -1*y_cmm_corr
            # Chi2 for FL's
            for l in dico_sim:
                i = list(dico_sim.keys()).index(l)
                y_fl_corr = dico_sim[l][4] + pars[2*(i+1)] + pars[2*i+3]*x
                dico_corr[l] = -1*y_fl_corr
            res_cmm = calculate_res_x(dico_corr)
            chi2 += (res_cmm-res_c)**2 # Need to be changed to include (sigma)**2
    return chi2

###################################################################################
###################################################################################

# Reconstructs the track on a given layer 
def y_otherlay_xray(gv, fl1, fl2, dico_sim):
    z1,z2 = z_coord(fl1), z_coord(fl2)
    y1,y2 = dico_sim[fl1][2]-dico_sim[fl1][3], dico_sim[fl2][2]-dico_sim[fl2][3]
    track_sl = (y1-y2) / (z1-z2)
    track_of = y1 - z1 * track_sl
    return track_sl*z_coord(gv) + track_of


# Takes in account the space between the layers
def z_coord(layer):
    return 10.97*layer-27.425


# Search for other detections of the same xray event
def find_similar(run_id, dico_others):
    dico_sim = {}
    for other in dico_others:
        for meas in dico_others[other]:
            if meas[0] == run_id: dico_sim[other] = meas    
    return dico_sim


# Returns the same dictionary without the chosen gv
def parse_other_layers(dico4, chosen_gv):
    dico_others = {}
    for elem1 in dico4:
        if elem1 != chosen_gv:
            dico_others[elem1] = dico4[elem1]
    return dico_others


# List of sums of other layers
def sum_others(gv,dico):
    list_layers = list(dico.keys())
    list_layers.remove(gv)
    list_sum_others = []
    for i in list_layers:
        for j in list_layers:
            if i<j:
                list_sum_others.append(i+j)
    return list_sum_others

def calculate_res_x(dico_y):
    z1,z2 = z_coord(list(dico_y.keys())[1]), z_coord(list(dico_y.keys())[2])
    y1,y2 = dico_y[list(dico_y.keys())[1]], dico_y[list(dico_y.keys())[2]]
    track_sl = (y1-y2) / (z1-z2)
    track_of = y1 - z1 * track_sl
    return track_sl*z_coord(z_coord(list(dico_y.keys())[0])) + track_of
