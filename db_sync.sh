#!/bin/bash

cur_dir=$(pwd)
mkdir -p qaqc_db
cd qaqc_db
for mod in sTGC-QC-{S,L}{1,2,3}
#for mod in sTGC-QC-S1
do
    if [ -d "$mod" ]
    then
	echo "Directory $mod found. Will update the XML files."
	cd $mod
	git status
	git pull origin XML2Excel
	cd -	
    else
	echo "Directory $mod was not found. Will clone the repository."
	git clone --single-branch -b XML2Excel ssh://git@gitlab.cern.ch:7999/atlas-muon-nsw-db/${mod}.git
    fi
done
cd $cur_dir
