# Modules
import sys, os, sqlite3, shlex, os.path
from xml.etree import ElementTree as ET
from board_transform import *
from math import *
from os import listdir
from os.path import isfile, join
import matplotlib.pyplot as plt
import scipy.optimize as spo
import statistics as stat

#!!! PAUL : import the file four_layers.py
import four_layers_cosm_xray as flcx
import warnings


####################
# GLOBAL VARIABLES #
####################
db_dir = 'qaqc_db'

####################
# HELPER FUNCTIONS #
####################
def get_smtf(mtf): # 'short mtf'
    return '%s%02i' % (mtf[:4], int(mtf[4:]))

def myfloat(val):
    if val == None:
        return None
    elif type(val)==float:
        return val
    elif type(val)==str and val.isspace():
        return None
    else:
        try:
            conv_val = float(val)
        except Exception:
            return None
        return conv_val

def check_dir(d):
    if not os.path.isdir(d): 
        raise Exception('Cannot locate directory "%s"' % (d))


##############
# XML READER #
##############
def open_xml(xml_file_name):
    return ET.parse(xml_file_name)

def read_xml(node_path, xml_tree):
    root = xml_tree.getroot()
    node = root.find(node_path)
    if node is None: raise Exception('Could not find node: ', node_path)
    return node.text

def read_xml_all(node_path, xml_tree):
    root = xml_tree.getroot()
    node = root.findall(node_path)
    if node is None: raise Exception('Could not find node: ', node_path)
    return [child.text for child in node]


##############
# X-RAY DATA #
##############
xray_ctypes = {'RUN_ID':str, 'MODULE':str, 'GAS_VOLUME':int, 'X_BEAM':float,\
               'Y_BEAM':float, 'Y_MEAS':float, 'Y_MEAS_ERROR':float}
class XRayEntry(object):
    def __init__(self):
        for k in xray_ctypes: setattr(self, k, None)
    def __repr__(self):
        return ' '.join(['%s=%s'%(k,str(v)) for k,v in self.__dict__.items()])


def parse_xray_file(fname):
    fin = open(fname, 'r')
    fdata = [l[:-1].split(',') for l in fin]
    fin.close()
    cols, fdata = fdata[0], fdata[1:]
    if set(cols)!=set(xray_ctypes.keys()):
        raise Exception('Invalid columns in file "%s" (%s). Expecting %s.' \
                        % (fname, cols, list(xray_ctypes.keys())))

    xentries = []
    for row in fdata:
        e = XRayEntry()
        for col_name, val in zip(cols, row):
            setattr(e, col_name, xray_ctypes[col_name](val))
        xentries.append(e)
    return xentries


def parse_xray_dataset(xray_data_dir):
    check_dir(xray_data_dir)

    flist = [f for f in listdir(xray_data_dir) if isfile(join(xray_data_dir, f))]
    flist = [join(xray_data_dir, f) for f in flist if f.endswith('.csv') and len(f)==10]
    print('Found %i X-ray files' % (len(flist)))
    xray_data = []
    for f in flist: xray_data.extend(parse_xray_file(f))
    print('Found %i X-ray measurements in dataset.' % (len(xray_data)))
    return xray_data


###############
# COSMIC DATA #
###############
cosmics_ctypes = {'X_ray_pt_id':str, 'Layer':int, 'Fixed_layer_1':int,\
        'Fixed_layer_2':int, 'x':float, 'y':float, 'x_low':float,'x_high': float,\
        'y_low':float, 'y_high':float,'x_ray_residual':float,\
        'xray_residual_error':float, 'fit_result':int, 'nEntries':int,\
        'Constant':float, 'Constant_error':float, 'Mean':float,\
        'Mean_error':float, 'Sigma':float, 'Sigma_error':float}
class CosmicsEntry(object):
    def __init__(self):
        for k in cosmics_ctypes: setattr(self, k, None)
    def define_file(self,file_name):
        self.mod_sn = file_name
    def __repr__(self):
        return ' '.join(['%s=%s'%(k,str(v)) for k,v in self.__dict__.items()])


def parse_cosmics_file(fname):
    fin = open(fname, 'r')
    fdata = [l[:-1].split(',') for l in fin]
    fin.close()
    cols, fdata = fdata[0], fdata[1:]
    for elem in range(len(cols)):
        cols[elem] = cols[elem].replace(' ','_')
        cols[elem] = cols[elem].replace('-','_')
    if set(cols)!=set(cosmics_ctypes.keys()):
        raise Exception('Invalid columns in file "%s" (%s). Expecting %s.' \
                        % (fname, cols, list(cosmics_ctypes.keys())))

    xentries = []
    for row in fdata:
        e = CosmicsEntry()
        for col_name, val in zip(cols, row):
            setattr(e, col_name, cosmics_ctypes[col_name](val))
        e.define_file(fname.split('/')[1][0:6])
        xentries.append(e)
    return xentries


def parse_cosmics_dataset(cosmics_data_dir):
    check_dir(cosmics_data_dir)

    flist = [f for f in listdir(cosmics_data_dir) if isfile(join(cosmics_data_dir, f))]
    flist = [join(cosmics_data_dir, f) for f in flist if f.endswith('.csv')]
    print('Found %i Cosmics files' % (len(flist)))
    cosmics_data = []
    for f in flist:
        cosmics_data.extend(parse_cosmics_file(f))
    print('Found %i Cosmics measurements in dataset.' % (len(cosmics_data)))
    return cosmics_data


######################
# QA/QC DB INTERFACE #
######################
def get_wedge_parenting(parenting_file):
    fin = open(parenting_file, 'r')
    fdata = [l[:-1].split() for l in fin]
    fin.close()
    return {l[0][5:]:{int(sn[2]):sn for sn in l[1].split(',')} for l in fdata}

def get_children(xml_file, children_eqtype):
    xml_tree = open_xml(xml_file)
    sn = xml_file.split('/')[-1].split('.xml')[0]
    parenting = read_xml_all(sn+'/parenting/value',  xml_tree)
    return [row.split(',')[1] for row in parenting if row.split(',')[0]==children_eqtype]


def get_qc_dir(mod_type):
    d = '%s/sTGC-QC-%s' % (db_dir, mod_type)
    check_dir(d)
    return d

    
def get_strip_board_sn(mod_sn, gv):
    try:
        qc_dir = get_qc_dir(mod_sn[1:3])

        doublets = get_children('%s/quadruplets/%s.xml' % (qc_dir, mod_sn), 'Doublets')
        doublet_sn = [d for d in doublets if str(gv) in d.split('.')[1][1:3]][0]
    
        gvs = get_children('%s/doublets/%s.xml' % (qc_dir, doublet_sn), 'Gas volumes')
        gv_sn = [g for g in gvs if str(gv) in g.split('.')[1][1:3]][0]

        hs_sn = get_children('%s/gas_volumes/%s.xml' % (qc_dir, gv_sn), 'Half-strips')[0]
        strip_board_sn = get_children('%s/half_strips/%s.xml' % (qc_dir, hs_sn), 'Strip plates')[0]
    except:
        return None
    return strip_board_sn    

cmm_meas_nodes = {'ofst':'CATS_STRIP_OFFSET', 'scl':'CATS_STRIP_SCALE', \
                 'par': 'CATS_STRIP_PARALLELISM', 'rot':'CATS_STRIP_ROTATION'}
metadata_keys = ['CMM_DATA_ORIGIN', 'NOM_PARS_TYPE', 'HAS_RAW_CMM_DATA']
def get_cmm_data(strip_board_sn):
    xml_path = '%s/strip_plates/%s.xml' % (get_qc_dir(strip_board_sn[0:2]), strip_board_sn)
    xml_tree = open_xml(xml_path)
    cmm_data = dict.fromkeys(metadata_keys+list(cmm_meas_nodes.keys()), None)

    # Get measurement metadata
    node = strip_board_sn+'/measurements/CATS_QC_OBSERVATION_CONFIRMATION/measurement_value/value'
    obs = read_xml(node, xml_tree)
    split_obs = shlex.split(obs)
    for elem in split_obs:
        for mkey in metadata_keys:
            if mkey in elem:
                cmm_data[mkey] = elem.split('=')[1]

    # Get measurement values
    for meas_name, node_name in cmm_meas_nodes.items():
        node = '%s/measurements/%s/measurement_value/value' % (strip_board_sn, node_name)
        val = myfloat(read_xml(node, xml_tree))
        cmm_data[meas_name] = val
        
    return cmm_data


##########
# FITTER #
##########
# fit_data: list of tuple(x, y, dy_xray, dy_cmm, dy_xray_error, dy_cmm_error)
# pars: [offset, slope]
def chi2(pars, fit_data):
    chi2 = 0
    #    for x, y, dy_xray, dy_cmm, dy_xray_error, dy_cmm_error in fit_data:
    for pt_id, x, y, dy_xray, y_cmm, dy_xray_error, dy_cmm_error in fit_data:
        y_cmm_corr = y_cmm + pars[0] + pars[1]*x
        dy_cmm_corr = y_cmm_corr - y
        diff = dy_cmm_corr-dy_xray
        chi2 += diff**2 / (dy_xray_error**2+dy_cmm_error**2)
    return chi2

def chi2_interchange(pars, fit_data):
    chi2 = 0
    for pt_id, x, y, dy_xray, y_cmm, dy_xray_error, dy_cmm_error in fit_data:
        y_xray = y - dy_xray
        dy_cmm = y - y_cmm
        y_xray_corr = y_xray + pars[0] + pars[1]*x
        dy_xray_corr = y_xray_corr - y
        diff = dy_xray_corr - dy_cmm
        chi2 += diff**2 / (dy_xray_error**2+dy_cmm_error**2)


########################
# RESULTS DB INTERFACE #
########################
def insert_db_entry(db_file_name, entry):
    if None in entry.values(): raise Exception('Invalid DB entry:', entry)
    mtf, module, gv = entry['MTF'], entry['MODULE'], entry['GAS_VOLUME']    
    conn = sqlite3.connect(db_file_name)

    # Create "FIT_RESULTS" table
    c = conn.cursor()
    c.execute('CREATE TABLE IF NOT EXISTS FIT_RESULTS'+
              '(MTF TEXT NOT NULL, MODULE TEXT NOT NULL, GAS_VOLUME INTEGER NOT NULL, '+\
              'SLOPE_FIT REAL, OFFSET_FIT REAL, CHI2 REAL, NDF INTEGER, '+\
              'OFFSET_CMM REAL, ROT_RAD_CMM REAL, SCALE_CMM REAL, NON_PAR_CMM REAL);')
    
    # Check if this entry already exists
    c = conn.cursor()
    res = c.execute("SELECT * FROM FIT_RESULTS WHERE MTF='%s' AND MODULE='%s' AND GAS_VOLUME=%i" % (mtf, mod_type, gv))
    res = list(res)
    if len(res)!=0:
        print('This gas volumes was already found in the database. Will overwrite...')
        c = conn.cursor()
        res = c.execute("DELETE FROM FIT_RESULTS WHERE MTF='%s' AND MODULE='%s' AND GAS_VOLUME=%i" % (mtf, mod_type, gv))

    # Insert the entry
    c = conn.cursor()
    cols_str = ','.join(entry.keys())
    vals_str = ','.join([ "'%s'"%(v) if k in ['MTF', 'MODULE'] else str(v) for k, v in entry.items()])
    cmd = 'INSERT INTO FIT_RESULTS (%s) VALUES (%s);' % (cols_str, vals_str)
    c.execute(cmd)

    # Close the DB
    conn.commit()
    conn.close()

################
# USE ONLY 1LM #
################

# Take in input a dictionary with 4 layers and return a dictionary with 3 layers
def use1lag(gv, dicox, dicoc):
    list_layers = [1,2,3,4]
    list_layers.remove(gv)
    for i in list_layers:
        if i!=max(list_layers) and i!=min(list_layers):
            list_layers.remove(i)
    dico_1lag_c = {gv:dicoc[chosen_gv]}
    dico_1lag_x = {gv:dicox[chosen_gv]}
    for l in list_layers:
        if l in dicox: dico_1lag_x[l]=dicox[l]
        if l in dicoc: dico_1lag_c[l]=dicoc[l]
    return dico_1lag_x, dico_1lag_c

##############################
# PARAMETERS WITHOUT COSMICS #
##############################

# Calculate the slope/offset for given by BenOC
def par_chi2_benoc(dico_layers_x):
    dico_par = {}
    for gv in dico_layers_x:
        min_res = spo.minimize(chi2, 2*[0], dico_layers_x[gv],\
                method='Nelder-mead', tol=0.0001, options={'maxiter':10000})
        dico_par[gv] = min_res.x
    return dico_par

#########################################
# PLOT THE CHI2 FUNCTION AT THE MINIMUM #
#########################################

def plot_chi2(par,dicox,dicoc,gv,function,path):
    par=list(par)
    offset,slope = par[0:2]
    l_sl,l_of,l_chi2_sl,l_chi2_of = [],[],[],[]
    for i in range(20000):
        of_p = offset + (i-10000)*1e-4
        sl_p = slope + (i-10000)*1e-7
        l_chi2_of.append(function([of_p,slope]+par[2:],dicox,dicoc,gv))
        l_chi2_sl.append(function([offset,sl_p]+par[2:],dicox,dicoc,gv))
        l_sl.append(1e3*sl_p), l_of.append(1e6*of_p)
    
    fig1=plt.figure(1,figsize=(20,7))
    
    ax1=plt.subplot(1,2,1)
    plt.plot(l_of,l_chi2_of)
    plt.ylabel('$\chi^2$',fontsize=15)
    plt.xlabel('Offset [µm]',fontsize=15)

    ax2=plt.subplot(1,2,2)
    plt.plot(l_sl,l_chi2_sl)
    plt.ylabel('$\chi^2$',fontsize=15)
    plt.xlabel('Slope [µrad]',fontsize=15)

    plt.savefig(path)


#################
# MAIN FUNCTION #
#################
if __name__=='__main__':
    #!!! PAUL : Don't need the gas volume in the .sh file
    usage = 'python3 stgc_as_built_fit_cosm+xray.py wedge_mtf module_id'
    if len(sys.argv)<3:
        print(usage)
        exit(0)

    mtf, mod_id = sys.argv[1:3]
    mod_id = int(mod_id)
    mod_type = 'Q%s%i%s' % (mtf[1], mod_id, mtf[3])
    print('**** stgc_as_built_fit_cosm+xray.py ****')
    print('MTF=%s module=%s' % (mtf, mod_type))
   
    #!!! PAUL : Create dictionary
    dico_4layers_x={}
    dico_4layers_c={}
    
    # Parse X-ray/Cosmics dataset + wedge parenting
    # (could be done only once if processing multiple boards)
    xdata = parse_xray_dataset('xray_data')
    cdata = parse_cosmics_dataset('cosmics')
    wparenting = get_wedge_parenting('wedge_parenting.txt')
    
    #!!! PAUL : Loop over every gv
    for gv in range(1,5):

        # Get the CMM and X-ray data
        if mtf not in wparenting: raise Exception('Cannot find %s in the parenting file.' % (mtf))
        if mod_id not in wparenting[mtf]: raise Exception('Cannot %s for mod_id=%i in the parenting file.' % (mtf,mod_id))
        mod_sn = wparenting[mtf][mod_id]
        
        #!!! PAUL : To have the same mod_sn for cosmics and for xray
        mod_sn_cosm = ''
        list_mod_sn = mod_sn.split('.')
        if len(list_mod_sn[-1]) == 1: list_mod_sn[-1] = '0' + list_mod_sn[-1]
        for elem in list_mod_sn: mod_sn_cosm += elem
        
        print('--GAS VOLUME = %i--'%(gv))

        strip_board_sn = get_strip_board_sn(mod_sn, gv)
        
        #!!! PAUL : Continue the for loop even if there is an error for 1 gv
        error_xray = False
        if strip_board_sn is None: 
            print('Cannot find strip board for module=%s gv=%i' % (mod_sn, gv))
            error_xray = True
        else:
            cmm_data = get_cmm_data(strip_board_sn)
            if None in cmm_data.values(): 
                print('CMM data is incomplete:', cmm_data)
                error_xray = True
            else:
                xray_data = [e for e in xdata if e.RUN_ID.startswith(mtf) and e.MODULE==mod_type and e.GAS_VOLUME==gv]
                if len(xray_data)==0:
                    print('No matching X-ray data was found.')
                    error_xray = True

        if not error_xray:

            #!!! PAUL : Same thing that it has been done for the xray with the cosmics
            cosmics_data = [e for e in cdata if e.mod_sn==mod_sn_cosm and int(e.Layer)==gv and e.fit_result==0]
            if len(cosmics_data)==0: raise Exception('No matching cosmics data was found.')


            print('STRIP_BOARD=%s MODULE=%s' % (strip_board_sn, mod_sn))

#            print('CMM data:', cmm_data)
#            print('X-ray data:', *xdata, sep='\n')
#	     print('Cosmics data:', *cosmics_data, sep='\n\n')


            # Get board position at each X-ray measurement
            nom_pars = nominal_parameters(mod_type, gv, cmm_data['CMM_DATA_ORIGIN'], cmm_data['NOM_PARS_TYPE'])
            offset, angle_rad, scale, non_par = cmm_data['ofst'], cmm_data['rot']/180*pi, cmm_data['scl'], cmm_data['par']
            transform = board_transform(nom_pars, gv, offset, angle_rad, scale, non_par)

            if   cmm_data['CMM_DATA_ORIGIN']=='WIS':  cmm_error=0.025
            elif cmm_data['CMM_DATA_ORIGIN']=='TRILABS':  cmm_error=0.015
            else: raise Exception('Invalid CMM_DATA_ORIGIN:', cmm_data['CMM_DATA_ORIGIN'])
            xray_error = 0.120


            # Combine the alignment data in one data structure (xray)
            combined_data_x = [] # tuple(run_id_xr, x, y, dy_xray, dy_cmm, dy_xray_error, dy_cmm_error)
            for e in xray_data:
                run_id_xr = ''
                ident = e.RUN_ID.split('_')
                pt_id = ident[2]+ident[3]
                x, y = e.X_BEAM, e.Y_BEAM
                dy_xray =  (e.Y_BEAM - e.Y_MEAS)
                x_cmm, y_cmm = transform.transform(x, y)
                combined_data_x.append((pt_id, x, y, dy_xray, y_cmm, xray_error, cmm_error))
#               print('x=%+.3f mm, y=%+.3f mm, dy_xray=%+.0f um, y_cmm=%+.3f um' % (x, y, 1000*dy_xray, y_cmm))
            
            # Combine the alignment data in one data structure (cosmics)
            combined_data_c = [] # tuple(run_id_xr, fixed layer 1, fixed layer 2, mean)
            for e in cosmics_data:
                pt_id = e.X_ray_pt_id
                fl1,fl2 = e.Fixed_layer_1, e.Fixed_layer_2
                mean = e.Mean
                combined_data_c.append((pt_id, fl1, fl2, mean))


            n = len(combined_data_x)
            ndf = n-2
#            print('Running least-square minimization on %i points (NDF=%i).' % (n, ndf))
            if ndf<0: print('Cannot run minimization. Not enough points (NDF=%i).' % (ndf))
        
            #!!! PAUL : Dictionary with the list of tuples that would normally go in the fit
            else:
                dico_4layers_x[gv] = combined_data_x

            dico_4layers_c[gv] = combined_data_c


    if len(dico_4layers_x)==0:
        print('!!! You don\'t have any layer to analyze for X-ray !!!')
        exit(0)

    #!!! PAUL : Ask the user which gv to analyze; will eventually be replaced by a for loop
    chosen_gv = int(input('Which gas volume do you want to analyze (Integer between 1 & 4)? '))
    while chosen_gv not in list(dico_4layers_x.keys()):
        chosen_gv = int(input('This gas volume cannot be analyzed. Please chose another one: '))

    dico_x, dico_c = use1lag(chosen_gv, dico_4layers_x, dico_4layers_c) # In order to use only 1 Lagrange multiplier
    
    # Values to give to the fit for the parameters before the minimization
    dico_par_benoc = par_chi2_benoc(dico_x)
    starting_values = [dico_par_benoc[chosen_gv][0],dico_par_benoc[chosen_gv][1]]
    liste = list(dico_par_benoc.keys())
    liste.remove(chosen_gv)
    starting_values += [dico_par_benoc[min(liste)][0],dico_par_benoc[min(liste)][1]]
    starting_values += [dico_par_benoc[max(liste)][0],dico_par_benoc[max(liste)][1]]
   

#   # Minimization of the chi2 function with the residuals as a constraint and 3LM
#    min_res = spo.minimize(flcx.chi2_cosm_xray, starting_values[0:2],\
#            args=(dico_4layers_x,dico_4layers_c,chosen_gv),\
#            method='Nelder-Mead', tol=0.0001, options={'maxiter':10000})

    # Run the fitter
    # The minimization of the chi2 residual chi2 function (cosm_res - cmm_res) need to be done with 3 layers
    min_res = spo.minimize(flcx.chi2_cosm_xray_flcorr, starting_values,\
            args=(dico_x, dico_c, chosen_gv),\
            method='Nelder-Mead', tol=0.0001, options={'maxiter':10000})

    ofst_fit,slope_fit = min_res.x[0:2]
    chi2=min_res.fun

    print('BenOC: ofst=%+i um, slope=%+i urad' % (1000*starting_values[0], 1000000*starting_values[1]))
    print('Residuals: ofst=%+i um, slope=%+i urad' % (1000*ofst_fit, 1000000*slope_fit))

    # Plot the chi2 function of your choice as a function of the slope and the offset
    plot_chi2(min_res.x,dico_x,dico_c,chosen_gv,flcx.chi2_cosm_xray_flcorr,'chi2_graph/chi2_cosm_xray_flcorr_%s.%i.pdf'%(mod_sn,chosen_gv))
    
    exit(0) # To avoid plotting histogram

    #### End of the modifications

    gv = chosen_gv

    residuals = []
    for x, y, dy_xray, y_cmm, dy_xray_error, dy_cmm_error in dico_x[gv]:
        y_cmm_corr = y_cmm + ofst_fit + slope_fit*x
        dy_cmm_corr = y_cmm_corr-y
        res = dy_xray-dy_cmm_corr
        residuals.append(1000*res)
        print('Fit residuals:', *['%+.0f um'%(x) for x in residuals], sep='\n' )

    # Make residual plot
    maxX = 1.2*max([abs(x) for x in residuals])
    plt.hist(residuals, bins=20, color='k', histtype='step', range=(-maxX, maxX))
    plt.xlabel(r'Fit residual [$\mu$m]')
    plt.ylabel('X-ray measurements')
    mu, rms = stat.mean(residuals), stat.stdev(residuals)
    smtf = get_smtf(mtf)
    t = r'%s_%s_GV%i $\mu=%f um \:\: \sigma=%f um$' % (smtf, mod_type, gv, mu, rms)
    plt.title(t, loc='left')

    #!!! PAUL : Change the file's location
    plt.savefig('pdf_try_chose/%s_%s_GV%i_residuals.pdf' % (smtf, mod_type, gv))


    # Save results to DB and file
    print('Saving fit results and CMM parameters to DB file: out_try_chose.db')
    db_entry = {'MTF':mtf, 'MODULE':mod_type, 'GAS_VOLUME':gv, 'SLOPE_FIT':slope_fit, 'OFFSET_FIT':ofst_fit,\
                'CHI2':chi2, 'NDF':ndf,
                'OFFSET_CMM':offset, 'ROT_RAD_CMM':angle_rad, 'SCALE_CMM':scale, 'NON_PAR_CMM':non_par}
    
    #!!! PAUL : Change the file name
    insert_db_entry('out_try_chose.db', db_entry)
    
    #!!! PAUL : Change the file's location
    fout_name = 'out_try_chose/%s_%s_GV%i_residuals.txt'  % (smtf, mod_type, gv)
    
    print('Writing residuals to file:', fout_name)
    fout = open(fout_name, 'w')
    for res in residuals: fout.write('%s,%s,%i,%f\n' % (mtf, mod_type, gv, res))
    fout.close()


