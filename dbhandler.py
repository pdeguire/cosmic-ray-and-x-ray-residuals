import cx_Oracle




# RETURN OPTIONS FOR DB SETUPS
# CONNECTION_TYPE -> [LXPLUS, LOCAL]
# DB_MODIFIER -> [PRODUCTION, PLAYGROUND]
# DB_NAME -> [STGC, LOG, VAL]
# ACTION -> [READ, WRITE]
def test_db_setups():
    for conn_type in db_setup.conn_type_opts:
        for db_modifier in db_setup.db_modifier_opts:
            for db_name in db_setup.db_name_opts:
                for action in db_setup.action_opts:
                    setup = db_setup(conn_type, db_modifier, db_name, action)
                    print(setup)
                    if not setup.isvalid(): raise Exception('Invalid setup.')


class db_setup(object):
    """Stores connection info to Oracle database."""
    
    # Static members
    conn_type_opts = ['LXPLUS', 'LOCAL']
    db_modifier_opts = ['PLAYGROUND', 'PRODUCTION']
    db_name_opts = ['STGC', 'LOG', 'VAL']
    action_opts = ['READ', 'WRITE']
    
    # Define setup based on options
    def __init__(self, conn_type, db_modifier, db_name, action):
        self.conn_type = conn_type
        self.db_modifier = db_modifier
        self.db_name = db_name
        self.action = action

        self.server = self.port = None
        if (conn_type, db_modifier)==('LXPLUS', 'PLAYGROUND'):
            self.server = "db-d0002.cern.ch"
            self.port   = "10654"
            self.domain = "int8r.cern.ch"
        elif (conn_type, db_modifier)==('LXPLUS', 'PRODUCTION'):
            self.server = "atlr-s.cern.ch"
            self.port   = "10121"
            self.domain = "atlr.cern.ch"
        elif (conn_type, db_modifier)==('LOCAL', 'PLAYGROUND'):
            self.server = "localhost.cern.ch"
            self.port   = "10005"
            self.domain = "int8r.cern.ch"
        elif (conn_type, db_modifier)==('LOCAL', 'PRODUCTION'):
            self.server  = "localhost.cern.ch"
            self.port    = "10004"
            self.domain  = "atlr.cern.ch"

        self.user = self.owner = self.pwd = None
        if (db_name,action)==('STGC','READ'):
            self.user   = "ATLAS_MUON_NSW_STGC_QAQC"
            self.owner  = "ATLAS_MUON_NSW_STGC_QAQC"
            self.pwd  = 'sTGC2020_QAQC_W' if db_modifier=='PLAYGROUND' else 'sTGC2020_QAQC_W_A'
        elif (db_name,action)==('STGC','WRITE'):
            self.user   = "ATLAS_MUON_NSW_STGC_QAQC_W"
            self.owner  = "ATLAS_MUON_NSW_STGC_QAQC"
            self.pwd  = 'sTGC2020_QAQC_W' if db_modifier=='PLAYGROUND' else 'sTGC2020_QAQC_W_A'

        elif (db_name,action)==('LOG','READ'):
            self.user  = "ATLAS_MUON_NSW_MM_READER"
            self.owner = "ATLAS_MUON_NSW_MM_LOG"
            self.pwd = 'NSWreadme2016'
        elif (db_name,action)==('LOG','WRITE'):
            self.user  = "ATLAS_MUON_NSW_MM_LOG_W"
            self.owner = "ATLAS_MUON_NSW_MM_LOG"
            self.pwd = 'FijdPadHgh.1' if db_modifier=='PLAYGROUND' else 'IhkAwImjwas...2'

        elif (db_name,action)==('VAL','READ'):
            self.user  = "ATLAS_MUON_NSW_MM_VAL_R"
            self.owner = "ATLAS_MUON_NSW_MM_VAL"
            self.pwd = 'READER2017VAL'
        elif (db_name,action)==('VAL','WRITE'): 
            self.user  = "ATLAS_MUON_NSW_MM_VAL_W"
            self.owner = "ATLAS_MUON_NSW_MM_VAL"
            self.pwd = 'WRITER2017VAL'

    def __repr__(self):
        return '\n'.join(['%s:\t%s' % (k, str(val)) for k, val in self.__dict__.items()])


    def isvalid(self):
        return not None in self.__dict__.values()
        
            
class db_handler(object):

    def __init__(self, setup, verbose=True):
       self.conn = None
       self.setup = setup
       self.verbose = verbose
       if not self.setup.isvalid():
           raise Exception('Invalid database setup. Cannot create handle.')
       self.open()
       
    def close(self):
        self.conn.close()

    def commit(self):
        if self.setup.action=='WRITE': self.conn.commit()
        else: print('WARNING: Current setup (READ) does not allow commit actions.')
		
    def execute(self, cmd):
        if self.verbose: print('DB EXEC:', cmd)
        self.cursor.execute(cmd)
	
    def open(self):
        """Initializes the database connection based on setup."""
        if self.verbose: print('Creating database connection using setup:\n%s' % (str(self.setup)))
        cmd = "(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = %s)(PORT = %s)) (LOAD_BALANCE = on)(CONNECT_DATA = (SERVER = DEDICATED) (SERVICE_NAME = %s) ) )" % \
        (self.setup.server, self.setup.port, self.setup.domain)
        self.conn = cx_Oracle.connect(self.setup.user, self.setup.pwd, cmd)
        self.cursor    = self.conn.cursor()
        self.execute('alter session set NLS_DATE_FORMAT ="DD.MM.YYYY HH24:MI:SS"')	
	
    def read(self, cmd):
        self.execute(cmd)
        cols = [info[0] for info in self.cursor.description]
        data = [row for row in self.cursor]
        return cols, data
