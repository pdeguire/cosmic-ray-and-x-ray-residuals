# Prints the wedge parenting
from dbhandler import *
import sys

def format_query(db_query):
    cols, table = db_query
    if len(table)==0: raise Exception('No entry found in DB query.')
    l = []
    for row in table:
        l.append(Entry())
        for idx, colname in enumerate(cols):
            setattr(l[-1], colname, row[idx])
    return l


class Entry(object):
    def __repr__(self):
        return '\t'.join(list(map(str, self.__dict__.values())))
    def process(self):
        pass


wdg_types = ['W%s%s%s'%(size,side,log) for size in ['L','S'] for side in ['A','C'] for log in ['P', 'C']]
mod_types = ['Q%s%i%s'%(size,id,log) for size in ['L','S'] for id in [1,2,3] for log in ['P', 'C']]

# Get the parenting table for wedges
def get_wedge_parenting(db):
    cmd = """
SELECT EQENTRYID CHILD_EQID, EQ.PARTSBATCHMTFID PARENT_MTF, EQTYPES.EQTYPECODE PARENT_EQTYPE 
FROM ATLAS_MUON_NSW_MM_LOG.PARENTING PARENTING
JOIN ATLAS_MUON_NSW_MM_LOG.EQUIPMENT EQ
ON EQ.ID_EQUIPMENT=PARENTEQENTRYID
JOIN ATLAS_MUON_NSW_MM_LOG.EQUIPMENTTYPES EQTYPES
ON EQTYPES.ID_EQUIPMENTTYPES=EQ.EQTYPECODEID
WHERE ISACTIVEFLAG='T' AND (EQTYPES.EQTYPECODE LIKE 'WL__' OR EQTYPES.EQTYPECODE LIKE 'WS__')
"""
    parenting = format_query(db.read(cmd))    
    parenting = [e for e in parenting if e.PARENT_EQTYPE in wdg_types]
    return parenting

def get_eqid_dictionary(db):
    cmd="""
SELECT ID_EQUIPMENT EQID, OTHERID ALIAS, EQTYPES.EQTYPECODE EQTYPE
FROM ATLAS_MUON_NSW_MM_LOG.EQUIPMENT EQ
JOIN ATLAS_MUON_NSW_MM_LOG.EQUIPMENTTYPES EQTYPES
ON EQTYPES.ID_EQUIPMENTTYPES=EQ.EQTYPECODEID
WHERE EQTYPES.EQTYPECODE LIKE 'WL__' OR EQTYPES.EQTYPECODE LIKE 'WL__'
OR EQTYPES.EQTYPECODE LIKE 'QL__' OR EQTYPES.EQTYPECODE LIKE 'QS__'
"""
    eqid_dict = format_query(db.read(cmd))
    types = wdg_types+mod_types
    eqid_dict = [e for e in eqid_dict if e.EQTYPE in types]
    eqid_dict = {e.EQID:e.ALIAS for e in eqid_dict}
    return eqid_dict
    

if __name__=='__main__':

    # Fetch data from the log DB
    #try:
        # Query the logistics DB
    conn_setup = db_setup('LOCAL', 'PRODUCTION', 'LOG', 'READ')
    db = db_handler(conn_setup, verbose=False)
    wparenting = get_wedge_parenting(db)
    eqid_dict = get_eqid_dictionary(db)
    db.close()
        
    # Assemble the wedge parenting
    wedges = dict()
    for e in wparenting:
        if e.CHILD_EQID in eqid_dict:
            sn = eqid_dict[e.CHILD_EQID]
            wedge_mtf = e.PARENT_MTF
            if wedge_mtf not in wedges:
                wedges[wedge_mtf]=[]
            wedges[wedge_mtf].append(sn)
                
#    except:
#        print('An error occured while querying the logistics database!', file=sys.stderr)
#        exit(1)

    # Check the parenting
    for k, l in wedges.items():
        if len(l)!=3:
            print('WARNING: Incomplete parenting for wedge %s' % (k), file=sys.stderr)
                
    # Print the parenting
    for k, l in wedges.items():
        print(k, ','.join(l))

    

    





    

