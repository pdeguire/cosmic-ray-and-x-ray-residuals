import sys, math
import matplotlib.pyplot as plt
import numpy as np
import statistics as stat
from scipy.optimize import curve_fit
from scipy import asarray as ar,exp
import sqlite3

def gaus(x,a,x0,sigma):
    return a*exp(-(x-x0)**2/(2*sigma**2))

def get_smtf(mtf): # 'short mtf'
    return '%s%02i' % (mtf[:4], int(mtf[4:]))


# Requires a list of fit residual files as argument
def make_residual_plots(argv):
    flist = argv
    print('In function "make_residual_plots"\nWill parse %i files.' % (len(flist)))
    residual_data = []
    for f in flist:
        fin = open(f, 'r')
        residual_data.extend([tuple(l[:-1].split(',')) for l in fin])
        fin.close()
    print('Found %i residual values.' % (len(residual_data)))

    for gv in [1,2,3,4]:

        # Plot histogram for this gas volume
        hist_data = [float(row[3]) for row in residual_data if int(row[2])==gv]
        mean, rms = stat.mean(hist_data), stat.stdev(hist_data)
        maxX = 5*rms
        maxX=800
        Y, X, patches = plt.hist(hist_data, bins=20, color='k', histtype='step', range=(-maxX,maxX), label='Data')
        X = [0.5*(x1+x2) for x1, x2 in zip(X[:-1], X[1:])]
        EY = [math.sqrt(y) for y in Y]
        plt.xlabel(r'Fit residual [$\mu$m]')
        plt.ylabel('X-ray measurements')
        

        # Make gaussian fit
        X, Y, EY = zip(*[(x,y,ey) for x,y,ey in zip(X,Y,EY) if y>0])
        popt, pcov = curve_fit(f=gaus,xdata=ar(X),ydata=ar(Y),sigma=ar(EY),p0=[max(Y),mean,rms])
        A_fit, mu_fit, sig_fit = popt
        
        Xfit = np.linspace(-maxX, maxX, 200)
        plt.plot(Xfit, gaus(Xfit,*popt),c='r',label='Fit')

        plt.title('GV%i\n'%(gv)+r'Fit result: A=%.2f, $\mu$=%.1f $\mu$m, $\sigma$=%.1f $\mu$m' \
                  % (A_fit, mu_fit, abs(sig_fit)), loc='left')
        
        # Save in PDF format
        plt.savefig('pdf/GV%i_residuals.pdf' % (gv))
        plt.clf()
    
        
    
# Requires a list of X-ray data files as argument
def make_statistics_plots(argv):
    cols = 'RUN_ID,MODULE,GAS_VOLUME,X_BEAM,Y_BEAM,Y_MEAS,Y_MEAS_ERROR'.split(',')
    flist = argv
    print('In function "make_statistics_plots"\nWill parse %i files.' % (len(flist)))
    pt_counter = dict()
    for f in flist:
        fin = open(f, 'r')
        fdata = [l[:-1] for l in fin]
        fdata = [l.split(',') for l in fdata]
        fcols = fdata.pop(0)
        if fcols != cols: raise Exception('Unexpected columns in file:', fcols)
        fin.close()

        for row in fdata:
            smtf, module, gv = get_smtf(row[0].split('_')[0]), row[1][0:3], int(row[2])
            gv_unique_id = '%s_%s_GV%i' % (smtf, module, gv)
            if gv_unique_id not in pt_counter: pt_counter[gv_unique_id]=1
            else: pt_counter[gv_unique_id] += 1


    mod_types = set([k.split('_')[1] for k in pt_counter.keys()])
    print('Module types:', mod_types)

    for mod_type in mod_types:
        hist_data = [v for k,v in pt_counter.items() if k.split('_')[1]==mod_type]
        maxX = int(max(hist_data))+2.5
        plt.hist(hist_data, bins=21, color='k', histtype='step', range=(-0.5,maxX))
        plt.xlabel('Good X-ray measurements')
        plt.ylabel('Gas volumes')
        plt.title('Module type: '+mod_type, loc='left')
        plt.xticks(np.arange(0, maxX+0.5, 1.0))
        plt.xlim((-0.5,maxX))
        plt.savefig('pdf/%s_statistics.pdf' % (mod_type))
        plt.clf()
    
        


def make_fit_stat_plots():

    # Get data in the DB
    cmd = "SELECT SLOPE_FIT, OFFSET_FIT FROM FIT_RESULTS;"
    conn = sqlite3.connect('out.db')
    c = conn.cursor()
    res = c.execute(cmd)
    fit_data = [tuple(map(float, e)) for e in res]
    conn.close()

    print('Found %i boards in DB.' % (len(fit_data)))

    fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(10,7))
    ylabels, mult, maxY = ['Strip board rotation (X-ray fit) [urad]', 'Strip board offset (X-ray fit) [um]'], [1000000, 1000], [1500, 500] 
    for idx, ax in enumerate(axes):
        Y = [mult[idx]*x[idx] for x in fit_data if abs(mult[idx]*x[idx])<maxY[idx]]
        N=len(Y)
        X = list(range(0,N))
        ax.scatter(X, Y, marker='o', c='k', s=8)
        ax.set_xlabel('Strip board')
        mu, stdev = stat.mean(Y), stat.stdev(Y)
        stats = r'$\mu=%.1f \:\: \sigma=%.1f$' % (mu, stdev)
        ax.set_ylim([-maxY[idx], maxY[idx]])
        ax.grid(linestyle='--')
        ax.set_ylabel(ylabels[idx])
        ax.set_title(stats, loc='left')

    plt.subplots_adjust(hspace=0.35)
    plt.savefig('pdf/fit_stats.pdf')
    plt.clf()

if __name__=='__main__':
    usage = """usage: python3 make_plots.py option args
  residual_dist list_of_residual_files...
  statistics list_of_xray_files...
  fit_stats"""
    if len(sys.argv)<2:
        print(usage)
        exit(0)

    opt = sys.argv[1]
    if   opt=='residual_dist': make_residual_plots(sys.argv[2:])
    elif opt=='statistics': make_statistics_plots(sys.argv[2:])
    elif opt=='fit_stats': make_fit_stat_plots()
    else: raise Exception('Option not recognized:', opt)
        
    
