# Import packages
import configparser
import math
import numpy as np
import random
from xml.etree import ElementTree as ET


# Constants for parsing the nominal geometry
# Taken from the Production X-ray analysis software
import configparser

nom_pars_file_name = 'NominalGeometry.ini'
class InvalidNomParArgs(Exception):
    pass


def get_board_type(gv):
    if gv in [1,3]:
        return 'K13'
    elif gv in [2,4]:
        return 'K24'
    else:
        raise Exception('Invalid gas volume.')

class nominal_parameters:
    """Stores the nominal parameters for a particular board type."""
    def __init__(self, module_type, gas_volume, origin, tag='DEFAULT'):
        if len(module_type)==4 and module_type[3] in ['P', 'C']:
            module_type = module_type[:3]

        if tag.upper()=='DEFAULT':
            sub_section = origin
        else:
            sub_section = origin+'_'+tag.upper()
        
        fconfig = configparser.ConfigParser()
        fconfig.read(nom_pars_file_name)
        self.section = module_type+'_'+sub_section
        if not self.section in fconfig.sections():
            raise InvalidNomParArgs('No matching key in configuration.');

        self.board_type = get_board_type(gas_volume)
        self.gas_volume = gas_volume
        self.origin = origin

        pars = fconfig[self.section]
        if len(pars)<11:
            raise InvalidNomParArgs('Found no nominal parameters for this board type and origin.')

        self.h = float(pars['h'])
        self.a = float(pars['a'])
        self.LSB = float(pars['LSB'])
        self.HB = float(pars['HB'])
        self.LV = float(pars['LV'])
        self.ang = float(pars['ang'])
        if self.board_type=='K13':
            self.alpha = float(pars['alpha_K13'])
            self.b = float(pars['b_K13'])
            self.f = float(pars['f_K13'])
        elif self.board_type=='K24':
            self.alpha = float(pars['alpha_K24'])
            self.b = float(pars['b_K24'])
            self.f = float(pars['f_K24'])
        else:
            raise Exception('Invalid board type.')
        self.type_str = module_type+"_"+self.board_type+"_"+origin

    def __repr__(self):
        return "[Nominal parameters for %s] h=%gmm a=%gmm LSB=%gmm LV=%gmm alpha=%gmm b=%gmm f=%gmm" % (self.type_str, self.h, self.a,  self.LSB, self.LV, self.alpha, self.b, self.f)

        
class board_transform:

    # Must specify the board type to properly do the flips
    # Possible board types are: 'K13' and 'K24'
    # Refer to 'cmm_formulas.pdf' for details of the board flips and signs
    # The general transform is deltaY = c[0]+c[1]x+c[2]y+c[3]xy
    # Units for all parameters should be millimeters and radians
    def __init__(self, nom_pars, gas_volume, ofst, delta_rad, scale, non_par):

        # Basic board information
        self.nom = nom_pars
        self.board_type = get_board_type(gas_volume)
        if self.board_type == 'K13':
            self.sng = 1
        elif self.board_type == 'K24':
            self.sng = -1
        else:
            raise Exception('Invalid board type.')

        # Non-conformities (and leftover geometry)
        self.gamma = ofst
        self.delta = delta_rad
        self.s = scale
        self.DP = non_par
        self.dist_DB_to_LS = self.nom.LV-self.nom.LSB-self.nom.b
        
        
        # Transform parameters: c0, c1, c2, c3
        self.c = 4*[0]
        self.c[0] = 0
        self.c[1] = -self.delta
        self.c[2] = self.s/self.nom.h
        self.c[3] = self.sng*self.DP/2/self.nom.alpha/self.nom.h
        self.gamma_corr = self.gamma - self.dy_strip_coordinates(-self.nom.a, self.nom.f-self.nom.b)
        self.c[0] = self.gamma_corr

        
    # Transformation in the short strip coordinate system
    def dy_strip_coordinates(self, x, y):
        return (self.c[0] + self.c[1]*x + self.c[2]*y + self.c[3]*x*y)

    # Transform in the module coordinate system
    def dy_module_coordinates(self, x, y):
        x_strip = x
        y_strip = self.dist_DB_to_LS - y
        return self.dy_strip_coordinates(x_strip, y_strip)

    # Full space point transform in the module coordinate system
    def transform(self, x, y):
        dy = self.dy_module_coordinates(x, y)
        return (x, y-dy)

    def __repr__(self):
        out = 'Board type=%s'%(self.board_type) + '\n'
        out += 'Parameters: gamma=%g delta=%g s=%g DP=%g dist_DB_to_LS=%g'%(self.gamma,self.delta,self.s,self.DP,self.dist_DB_to_LS) + '\n'
        out += 'Coefficients in LS frame: ' + ' '.join(["c%i=%g"%(idx,val) for idx,val in enumerate(self.c)])
        return out
    

def read_xml(node_path, xml_file_name):
    tree = ET.parse(xml_file_name)
    root = tree.getroot()
    node = root.find(node_path)
    return node.text

geometry_path = '../../geometry/'
if __name__ == '__main__':
    pass


            
    
    
            


    
