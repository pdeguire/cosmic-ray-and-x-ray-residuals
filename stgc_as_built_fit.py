# Modules
import sys, os, sqlite3, shlex, os.path
from xml.etree import ElementTree as ET
from board_transform import *
from math import *
from os import listdir
from os.path import isfile, join
import matplotlib.pyplot as plt
import scipy.optimize as spo
import statistics as stat


####################
# GLOBAL VARIABLES #
####################
db_dir = 'qaqc_db'

####################
# HELPER FUNCTIONS #
####################
def get_smtf(mtf): # 'short mtf'
    return '%s%02i' % (mtf[:4], int(mtf[4:]))

def myfloat(val):
    if val == None:
        return None
    elif type(val)==float:
        return val
    elif type(val)==str and val.isspace():
        return None
    else:
        try:
            conv_val = float(val)
        except Exception:
            return None
        return conv_val

def check_dir(d):
    if not os.path.isdir(d): 
        raise Exception('Cannot locate directory "%s"' % (d))


##############
# XML READER #
##############
def open_xml(xml_file_name):
    return ET.parse(xml_file_name)

def read_xml(node_path, xml_tree):
    root = xml_tree.getroot()
    node = root.find(node_path)
    if node is None: raise Exception('Could not find node: ', node_path)
    return node.text

def read_xml_all(node_path, xml_tree):
    root = xml_tree.getroot()
    node = root.findall(node_path)
    if node is None: raise Exception('Could not find node: ', node_path)
    return [child.text for child in node]


##############
# X-RAY DATA #
##############
xray_ctypes = {'RUN_ID':str, 'MODULE':str, 'GAS_VOLUME':int, 'X_BEAM':float,\
               'Y_BEAM':float, 'Y_MEAS':float, 'Y_MEAS_ERROR':float}
class XRayEntry(object):
    def __init__(self):
        for k in xray_ctypes: setattr(self, k, None)
    def __repr__(self):
        return ' '.join(['%s=%s'%(k,str(v)) for k,v in self.__dict__.items()])


def parse_xray_file(fname):
    fin = open(fname, 'r')
    fdata = [l[:-1].split(',') for l in fin]
    fin.close()
    cols, fdata = fdata[0], fdata[1:]
    if set(cols)!=set(xray_ctypes.keys()):
        raise Exception('Invalid columns in file "%s" (%s). Expecting %s.' \
                        % (fname, cols, list(xray_ctypes.keys())))

    xentries = []
    for row in fdata:
        e = XRayEntry()
        for col_name, val in zip(cols, row):
            setattr(e, col_name, xray_ctypes[col_name](val))
        xentries.append(e)
    return xentries


def parse_xray_dataset(xray_data_dir):
    check_dir(xray_data_dir)

    flist = [f for f in listdir(xray_data_dir) if isfile(join(xray_data_dir, f))]
    flist = [join(xray_data_dir, f) for f in flist if f.endswith('.csv') and len(f)==10]
    print('Found %i X-ray files' % (len(flist)))
    xray_data = []
    for f in flist: xray_data.extend(parse_xray_file(f))
    print('Found %i X-ray measurements in dataset.' % (len(xray_data)))
    return xray_data



######################
# QA/QC DB INTERFACE #
######################
def get_wedge_parenting(parenting_file):
    fin = open(parenting_file, 'r')
    fdata = [l[:-1].split() for l in fin]
    fin.close()
    return {l[0][5:]:{int(sn[2]):sn for sn in l[1].split(',')} for l in fdata}

def get_children(xml_file, children_eqtype):
    xml_tree = open_xml(xml_file)
    sn = xml_file.split('/')[-1].split('.xml')[0]
    parenting = read_xml_all(sn+'/parenting/value',  xml_tree)
    return [row.split(',')[1] for row in parenting if row.split(',')[0]==children_eqtype]


def get_qc_dir(mod_type):
    d = '%s/sTGC-QC-%s' % (db_dir, mod_type)
    check_dir(d)
    return d

    
def get_strip_board_sn(mod_sn, gv):
    try:
        qc_dir = get_qc_dir(mod_sn[1:3])
    
        doublets = get_children('%s/quadruplets/%s.xml' % (qc_dir, mod_sn), 'Doublets')
        doublet_sn = [d for d in doublets if str(gv) in d.split('.')[1][1:3]][0]
    
        gvs = get_children('%s/doublets/%s.xml' % (qc_dir, doublet_sn), 'Gas volumes')
        gv_sn = [g for g in gvs if str(gv) in g.split('.')[1][1:3]][0]

        hs_sn = get_children('%s/gas_volumes/%s.xml' % (qc_dir, gv_sn), 'Half-strips')[0]
        strip_board_sn = get_children('%s/half_strips/%s.xml' % (qc_dir, hs_sn), 'Strip plates')[0]
    except:
        return None
    return strip_board_sn    

cmm_meas_nodes = {'ofst':'CATS_STRIP_OFFSET', 'scl':'CATS_STRIP_SCALE', \
                 'par': 'CATS_STRIP_PARALLELISM', 'rot':'CATS_STRIP_ROTATION'}
metadata_keys = ['CMM_DATA_ORIGIN', 'NOM_PARS_TYPE', 'HAS_RAW_CMM_DATA']
def get_cmm_data(strip_board_sn):
    xml_path = '%s/strip_plates/%s.xml' % (get_qc_dir(strip_board_sn[0:2]), strip_board_sn)
    xml_tree = open_xml(xml_path)
    cmm_data = dict.fromkeys(metadata_keys+list(cmm_meas_nodes.keys()), None)

    # Get measurement metadata
    node = strip_board_sn+'/measurements/CATS_QC_OBSERVATION_CONFIRMATION/measurement_value/value'
    obs = read_xml(node, xml_tree)
    split_obs = shlex.split(obs)
    for elem in split_obs:
        for mkey in metadata_keys:
            if mkey in elem:
                cmm_data[mkey] = elem.split('=')[1]

    # Get measurement values
    for meas_name, node_name in cmm_meas_nodes.items():
        node = '%s/measurements/%s/measurement_value/value' % (strip_board_sn, node_name)
        val = myfloat(read_xml(node, xml_tree))
        cmm_data[meas_name] = val
        
    return cmm_data


##########
# FITTER #
##########
# fit_data: list of tuple(x, y, dy_xray, dy_cmm, dy_xray_error, dy_cmm_error)
# pars: [offset, slope]
def chi2(pars, fit_data):
    chi2 = 0
    #    for x, y, dy_xray, dy_cmm, dy_xray_error, dy_cmm_error in fit_data:
    for x, y, dy_xray, y_cmm, dy_xray_error, dy_cmm_error in fit_data:
        y_cmm_corr = y_cmm + pars[0] + pars[1]*x
        dy_cmm_corr = y_cmm_corr-y
        diff = dy_cmm_corr-dy_xray
        chi2 += diff**2 / (dy_xray_error**2+dy_cmm_error**2)
    return chi2

########################
# RESULTS DB INTERFACE #
########################
def insert_db_entry(db_file_name, entry):
    if None in entry.values(): raise Exception('Invalid DB entry:', entry)
    mtf, module, gv = entry['MTF'], entry['MODULE'], entry['GAS_VOLUME']    
    conn = sqlite3.connect(db_file_name)

    # Create "FIT_RESULTS" table
    c = conn.cursor()
    c.execute('CREATE TABLE IF NOT EXISTS FIT_RESULTS'+
              '(MTF TEXT NOT NULL, MODULE TEXT NOT NULL, GAS_VOLUME INTEGER NOT NULL, '+\
              'SLOPE_FIT REAL, OFFSET_FIT REAL, CHI2 REAL, NDF INTEGER, '+\
              'OFFSET_CMM REAL, ROT_RAD_CMM REAL, SCALE_CMM REAL, NON_PAR_CMM REAL);')
    
    # Check if this entry already exists
    c = conn.cursor()
    res = c.execute("SELECT * FROM FIT_RESULTS WHERE MTF='%s' AND MODULE='%s' AND GAS_VOLUME=%i" % (mtf, mod_type, gv))
    res = list(res)
    if len(res)!=0:
        print('This gas volumes was already found in the database. Will overwrite...')
        c = conn.cursor()
        res = c.execute("DELETE FROM FIT_RESULTS WHERE MTF='%s' AND MODULE='%s' AND GAS_VOLUME=%i" % (mtf, mod_type, gv))

    # Insert the entry
    c = conn.cursor()
    cols_str = ','.join(entry.keys())
    vals_str = ','.join([ "'%s'"%(v) if k in ['MTF', 'MODULE'] else str(v) for k, v in entry.items()])
    cmd = 'INSERT INTO FIT_RESULTS (%s) VALUES (%s);' % (cols_str, vals_str)
    c.execute(cmd)

    # Close the DB
    conn.commit()
    conn.close()
    

#################
# MAIN FUNCTION #
#################
if __name__=='__main__':
    usage = 'python3 stgc_as_built_fit.py wedge_mtf module_id gas_volume'
    if len(sys.argv)<4:
        print(usage)
        exit(0)

    mtf, mod_id, gv = sys.argv[1:4]
    mod_id, gv = int(mod_id), int(gv)
    mod_type = 'Q%s%i%s' % (mtf[1], mod_id, mtf[3])
    print('**** stgc_as_built_fit.py ****')
    print('MTF=%s module=%s gv=%i' % (mtf, mod_type, gv))

    # Parse X-ray dataset + wedge parenting
    # (could be done only once if processing multiple boards)
    xdata = parse_xray_dataset('xray_data')
    wparenting = get_wedge_parenting('wedge_parenting.txt')



    # Get the CMM and X-ray data
    if mtf not in wparenting: raise Exception('Cannot find %s in the parenting file.' % (mtf))
    if mod_id not in wparenting[mtf]: raise Exception('Cannot %s for mod_id=%i in the parenting file.' % (mtf,mod_id))
    mod_sn = wparenting[mtf][mod_id]
    strip_board_sn = get_strip_board_sn(mod_sn, gv)
    if strip_board_sn is None: raise Exception('Cannot find strip board for module=%s gv=%i' % (mod_sn, gv))
    print('STRIP_BOARD=%s MODULE=%s' % (strip_board_sn, mod_sn))
    cmm_data = get_cmm_data(strip_board_sn)
    if None in cmm_data.values(): raise Exception('CMM data is incomplete:', cmm_data)
    
    xdata = [e for e in xdata if e.RUN_ID.startswith(mtf) and e.MODULE==mod_type and e.GAS_VOLUME==gv]
    if len(xdata)==0: raise Exception('No matching X-ray data was found.')

    print('CMM data:', cmm_data)
    print('X-ray data:', *xdata, sep='\n')


    # Get board position at each X-ray measurement
    nom_pars = nominal_parameters(mod_type, gv, cmm_data['CMM_DATA_ORIGIN'], cmm_data['NOM_PARS_TYPE'])
    offset, angle_rad, scale, non_par = cmm_data['ofst'], cmm_data['rot']/180*pi, cmm_data['scl'], cmm_data['par']
    transform = board_transform(nom_pars, gv, offset, angle_rad, scale, non_par)

    if   cmm_data['CMM_DATA_ORIGIN']=='WIS':  cmm_error=0.025
    elif cmm_data['CMM_DATA_ORIGIN']=='TRILABS':  cmm_error=0.015
    else: raise Exception('Invalid CMM_DATA_ORIGIN:', cmm_data['CMM_DATA_ORIGIN'])
    xray_error = 0.120


    # Combine the alignment data in one data structure
    combined_data = [] # tuple(x, y, dy_xray, dy_cmm, dy_xray_error, dy_cmm_error)
    for e in xdata:
        x, y = e.X_BEAM, e.Y_BEAM
        dy_xray =  (e.Y_BEAM - e.Y_MEAS)
        x_cmm, y_cmm = transform.transform(x, y)
        combined_data.append((x, y, dy_xray, y_cmm, xray_error, cmm_error))
        print('x=%+.3f mm, y=%+.3f mm, dy_xray=%+.0f um, y_cmm=%+.3f um' % (x, y, 1000*dy_xray, y_cmm))

    n = len(combined_data)
    ndf = n-2
    print('Running least-square minimization on %i points (NDF=%i).' % (n, ndf))
    if ndf<0: raise Exception('Cannot run minimization. Not enough points (NDF=%i).' % (ndf))

    
    # Run the fitter
    min_res = spo.minimize(chi2, 2*[0], args=combined_data, method='Nelder-Mead', tol=0.0001, options={'maxiter':10000})    
    print('fit_result:', min_res)
    ofst_fit, slope_fit = min_res.x
    chi2 = min_res.fun
    print('ofst_fit=%+.0f um, slope_fit=%+.0f urad, chi2/ndf=%f' % (1000*ofst_fit, 1000000*slope_fit, chi2/ndf))

    # Calculate the residuals
    residuals = []
    for x, y, dy_xray, y_cmm, dy_xray_error, dy_cmm_error in combined_data:
        y_cmm_corr = y_cmm + ofst_fit + slope_fit*x
        dy_cmm_corr = y_cmm_corr-y
        res = dy_xray-dy_cmm_corr
        residuals.append(1000*res)
    print('Fit residuals:', *['%+.0f um'%(x) for x in residuals], sep='\n' )


    # Make residual plot
    maxX = 1.2*max([abs(x) for x in residuals])
    plt.hist(residuals, bins=20, color='k', histtype='step', range=(-maxX, maxX))
    plt.xlabel(r'Fit residual [$\mu$m]')
    plt.ylabel('X-ray measurements')
    mu, rms = stat.mean(residuals), stat.stdev(residuals)
    smtf = get_smtf(mtf)
    t = r'%s_%s_GV%i $\mu=%f um \:\: \sigma=%f um$' % (smtf, mod_type, gv, mu, rms)
    plt.title(t, loc='left')
    plt.savefig('pdf/%s_%s_GV%i_residuals.pdf' % (smtf, mod_type, gv))


    # Save results to DB and file
    print('Saving fit results and CMM parameters to DB file: out.db')
    db_entry = {'MTF':mtf, 'MODULE':mod_type, 'GAS_VOLUME':gv, 'SLOPE_FIT':slope_fit, 'OFFSET_FIT':ofst_fit,\
                'CHI2':chi2, 'NDF':ndf,
                'OFFSET_CMM':offset, 'ROT_RAD_CMM':angle_rad, 'SCALE_CMM':scale, 'NON_PAR_CMM':non_par}
    insert_db_entry('out.db', db_entry)

    fout_name = 'out/%s_%s_GV%i_residuals.txt'  % (smtf, mod_type, gv)
    print('Writing residuals to file:', fout_name)
    fout = open(fout_name, 'w')
    for res in residuals: fout.write('%s,%s,%i,%f\n' % (mtf, mod_type, gv, res))
    fout.close()



        

 
