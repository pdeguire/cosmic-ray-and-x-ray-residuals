#!/bin/bash

USER="pdeguire"

# 1. Update the wedge parenting
# Contact kim.temming@physik.uni-freiburg.de for trouble with Oracle DB connections
# Contact Estel if wedge parenting is incomplete
ssh -L10004:itrac5401-v.cern.ch:10121 ${USER}@lxplus.cern.ch # run this command in another terminal
python3 update_wedge_parenting.py > wedge_parenting.txt

# 2. Fetch the X-ray survey data on lxplus
mkdir -p xray_data
rsync ${USER}@lxplus.cern.ch:/eos/atlas/atlascerngroupdisk/det-nsw-stgc/xray_alignment_test/results/*.csv xray_data/

# 3. Clone the QA/QC database
source db_sync.sh

# 4. Run the script
#   For this example, a selection of good wedges is processed:
# Can also do: source run_all.sh
for mtf in $(echo WLAC0000{2..8} WLAP0000{1,3,4,6,7,8})
do
    for id in {1..3}:
    do
	for gv in {1..4}:
	do
	    python3 stgc_as_built_fit.py $mtf $id $gv
	done
    done
done



# 4. Dump the results to a CSV file (optional)
echo "SELECT * FROM FIT_RESULTS;" | sqlite3 out.db -header -separator "," > out.csv

# 5. Summary and statistic plots
python3 make_plots.py statistics xray_data/*.csv
python3 make_plots.py residual_dist out/*_residuals.txt
