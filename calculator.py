# Calculator class which transforms the strip position
# based on the content of the out.db SQLite3 DB
# Must run the full stgc-as-built.py analyis beforehand.

import sys, sqlite3
from board_transform import *
from stgc_as_built_fit import *


def find_mtf_from_mod_sn(wedge_parenting, mod_sn):
    for mtf, mods in wedge_parenting.items():
        if mod_sn in mods.values():
            return mtf
    return None




def query_fit_result(mtf, mod_type, gv):
    cmd = "SELECT SLOPE_FIT, OFFSET_FIT FROM FIT_RESULTS WHERE MTF='%s' AND MODULE='%s' AND GAS_VOLUME=%i;"\
        % (mtf, mod_type, gv)
    conn = sqlite3.connect('out.db')
    c = conn.cursor()
    res = c.execute(cmd)
    res = [e for e in res]
    if len(res)!=1: return None
    else: return (float(res[0][0]), float(res[0][1]))
    conn.close()

    
    
# To initialize calculator:
#   provide mtf and mod_id [1,2,3]
#   or
#   provide mod_sn
#   gv is always mandatory
class calculator(object):
    def __init__(self, mtf=None, mod_id=None, mod_sn=None, gv=None):
        # Validate arguments:
        if (gv is None) or all([x is None for x in [mtf,mod_id,mod_sn]]):
            raise Exception('Invalid initialization of the calculator object.')

        wparenting = get_wedge_parenting('wedge_parenting.txt')
        
        if mtf is None: mtf=find_mtf_from_mod_sn(wparenting, mod_sn)
        if mtf is None: raise Exception('Could not find this module in the wedge parenting:', mod_sn)

        if mod_id is None: mod_id = mod_sn[2] if len(mod_sn)>=7 else None
        if mod_id is None: raise Exception('Invalid module sn:', mod_sn)

        self.mtf = mtf
        self.mod_type = 'Q%s%s%s' % (mtf[1], str(mod_id), mtf[3])
        self.gv = int(gv)
        self.mod_id = int(mod_id)
        self.mod_sn = mod_sn

        #print('Creating calculator object for: mtf=%s mod_type=%s gv=%i' % (self.mtf, self.mod_type, self.gv))

        # Query alignment fit results
        fit_res = query_fit_result(self.mtf, self.mod_type, self.gv)
        if fit_res is None: raise Exception('Did not find as-built fit results for this mtf/mod/gv.')
        self.slope_fit, self.offset_fit = fit_res

        # Initialize board_transform object
        if self.mod_sn is None: self.mod_sn = wparenting[self.mtf][self.mod_id]
        self.strip_board_sn = get_strip_board_sn(self.mod_sn, self.gv)
        if self.strip_board_sn is None: raise Exception('Cannot find strip board for module=%s gv=%i' % (self.mod_sn, self.gv))
        cmm_data = get_cmm_data(self.strip_board_sn)
        if None in cmm_data.values(): raise Exception('CMM data is incomplete:', cmm_data)
        
        nom_pars = nominal_parameters(self.mod_type, self.gv, cmm_data['CMM_DATA_ORIGIN'], cmm_data['NOM_PARS_TYPE'])
        offset_cmm, angle_rad_cmm, scale_cmm, non_par_cmm = \
            cmm_data['ofst'], cmm_data['rot']/180*pi, cmm_data['scl'], cmm_data['par']
        self.btransform = board_transform(nom_pars, self.gv, offset_cmm, angle_rad_cmm, scale_cmm, non_par_cmm)

    def __repr__(self):
        return '[calculator object]:\n'+'\n'.join(['%s: %s'%(k,str(v)) for k, v in self.__dict__.items()])


    def transform(self, x_nom, y_nom):
        x_cmm, y_cmm = self.btransform.transform(x_nom, y_nom)
        return (y_cmm + self.offset_fit + self.slope_fit*x_cmm)
        

if __name__=='__main__':
    usage ="""usage: 
  python3 calculator.py mtf mod_id gv x_nom y_nom
  python3 calculator.py mod_sn gv x_nom y_nom"""
    argv, argc = sys.argv, len(sys.argv)
    if argc==6:
        c = calculator(mtf=argv[1], mod_id=argv[2], gv=argv[3])
        x_nom, y_nom = float(argv[4]), float(argv[5])
    elif argc==5:
        c = calculator(mod_sn=argv[1], gv=argv[2])
        x_nom, y_nom = float(argv[3]), float(argv[4])
    else:
        print(usage)
        exit(0)

        
    #print(c)
    y_transform = c.transform(x_nom, y_nom)
    print('(%f, %f)  ->  (%f, %f)' % (x_nom, y_nom, x_nom, y_transform))

        
    


