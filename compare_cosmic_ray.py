# Comparison with cosmic-ray data

import sys, os
from calculator import *
import matplotlib.pyplot as plt
import statistics as stat
from math import *

z_dict = {1:-8.2, 2:-19.22, 3:-30.24, 4:-41.26}

if __name__=='__main__':
    usage = 'python3 compare_cosmic_ray.py module_sn'
    if len(sys.argv)<2:
        print(usage)
        exit(0)

    mod_sn = sys.argv[1]
    print('**** compare_cosmic_ray.py ****')
    print('Module:', mod_sn)

    # Get the calculators for each gas volume
    print('Getting calculator for all gas volumes...')
    calculators={gv:calculator(mod_sn=mod_sn, gv=gv) for gv in [1,2,3,4]}
    for gv,c in calculators.items():
        print('GV%i slope=%furad ofst=%fum' % (gv, 1000000*c.slope_fit, 1000*c.offset_fit))

    
    # Find cosmic-ray results
    ssn = mod_sn.split('.')
    cr_file_name = 'cosmic_ray_data/%s%s%02i.table.txt' % (ssn[0], ssn[1], int(ssn[2]))
    print('Opening file:', cr_file_name)
    if not os.path.isfile(cr_file_name):
        raise Exception('Could not find CR results file:', cr_file_name)
    fin = open(cr_file_name, 'r')
    cr_data = [l[:-1] for l in fin]
    fin.close()
    cr_data = [l.split(',') for l in cr_data[1:]]
    cr_data = [[int(row[0])] + list(map(float, row[1:])) for row in cr_data]
    cr_data = [tuple(row) for row in cr_data]
    
    print('Found %i good cosmic-ray data points.' % (len(cr_data)))

    
    ref_gv = {2:(1,3), 3:(2,4)}
    cr_dev, xray_dev = {2:[], 3:[]}, {2:[], 3:[]}
    

    for gv, minx, maxx, miny, maxy, mean, mean_error in cr_data:
        ref_gv1, ref_gv2 = ref_gv[gv]
        z, z_ref1, z_ref2 = z_dict[gv], z_dict[ref_gv1], z_dict[ref_gv2]

        mean_x, mean_y = 0.5*(minx+maxx), 0.5*(miny+maxy)

        # Corrected position of the strips
        y_tr = calculators[gv].transform(mean_x, mean_y)
        y_tr_ref1 = calculators[ref_gv1].transform(mean_x, mean_y)
        y_tr_ref2 = calculators[ref_gv2].transform(mean_x, mean_y)

        # Shift of the strips
        dy = y_tr - mean_y
        dy_tr_ref1 = y_tr_ref1 - mean_y
        dy_tr_ref2 = y_tr_ref2 - mean_y

        # Measured position (minus the shift)
        y_tr_meas = mean_y - dy
        y_tr_ref1_meas = mean_y - dy_tr_ref1
        y_tr_ref2_meas = mean_y - dy_tr_ref2

        slope_ref = (y_tr_ref2_meas-y_tr_ref1_meas)/(z_ref2-z_ref1)
        offset_ref = y_tr_ref1_meas - slope_ref*z_ref1
        ex_residual = 1000*(y_tr_meas - (slope_ref*z+offset_ref) )

        cr_dev[gv].append(mean)
        xray_dev[gv].append(ex_residual)        
        print(mean_x, mean_y, ex_residual, mean)

    fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(10,10))
    for idx, gv in enumerate([2,3]):
        ax = axes[idx][0]
        X, Y = cr_dev[gv], xray_dev[gv]
        min_plot, max_plot = min(X+Y), max(X+Y)
        t = 'Module %s GV%i' % (mod_sn, gv)

        line = np.linspace(min_plot, max_plot, 100)
        
        
        ax.scatter(X, Y,  marker='o', c='k', s=8)
        #ax.errorbar(X, Y, yerr=len(X)*[140], xerr=0, mfc='k', lw=1, ecolor='k', marker='o', mew=0, ms=8, fmt='none')
        ax.plot(line, line, c='r')
        ax.set_xlabel('Cosmic-ray residual [um]')
        ax.set_ylabel('X-ray residual [um]')
        ax.set_title(t, loc='left')
        ax.grid(linestyle='--')

        # Histogram of differences
        ax = axes[idx][1]
        diff = [x-y for x,y in zip(X,Y)]
        mu, stdev, rms = stat.mean(diff), stat.stdev(diff), sqrt(sum([x**2 for x in diff])/len(diff)) 
        stats = r'$\mu=%.1f um \:\: \sigma=%.1f um$  rms=%.1f um' % (mu, stdev, rms)
        ax.hist(diff, bins=40, color='k', histtype='step', range=(-500, 500))
        ax.set_xlabel('Difference [um]')
        ax.set_ylabel('Points')
        ax.set_title('%s\n%s' % (t,stats), loc='left')

    plt.subplots_adjust(top=0.95, bottom=0.05, left=0.10, right=0.95, hspace=0.2, wspace=0.2)
    plt.savefig('pdf/%s_cr_compare.pdf' % (mod_sn))
    plt.clf()
    
